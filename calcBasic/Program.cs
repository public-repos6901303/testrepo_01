using System;

class Calculator
{
    static void Main()
    {
        Console.WriteLine("Prosty kalkulator");

        while (true)
        {
            Console.WriteLine("1. Dodawanie");
            Console.WriteLine("2. Odejmowanie (zaimplementowane w branchu feature-a)");
            Console.WriteLine("3. Mnożenie (zaimplementowane w branchu feature-b)");
            Console.WriteLine("4. Wyjście z programu");
            Console.Write("Wybierz operację (1-4): ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Add();
                    break;
                case "2":
                    Subtract();
                    break;
                case "3":
                    Multiply();
                    break;                
                case "4":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Niepoprawny wybór. Spróbuj ponownie.");
                    break;
            }

            Console.WriteLine("------------------------");
        }
    }
    static void Add()
    {
        Console.Write("Podaj pierwszą liczbę: ");
        double num1 = Convert.ToDouble(Console.ReadLine());

        Console.Write("Podaj drugą liczbę: ");
        double num2 = Convert.ToDouble(Console.ReadLine());

        double result = num1 + num2;
        Console.WriteLine($"Wynik dodawania: {result}");
    }
    static void Subtract()
    {
        Console.Write("Podaj pierwszą liczbę: ");
        double num1 = Convert.ToDouble(Console.ReadLine());

        Console.Write("Podaj drugą liczbę: ");
        double num2 = Convert.ToDouble(Console.ReadLine());

        double result = num1 - num2;
        Console.WriteLine($"Wynik odejmowania: {result}");
    }
    static void Multiply()
    {
        Console.Write("Podaj pierwszą liczbę: ");
        double num1 = Convert.ToDouble(Console.ReadLine());

        Console.Write("Podaj drugą liczbę: ");
        double num2 = Convert.ToDouble(Console.ReadLine());

        double result = num1 * num2;
        Console.WriteLine($"Wynik mnożenia: {result}");
    }
}

